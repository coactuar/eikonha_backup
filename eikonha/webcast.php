<?php
	require_once "config.php";
	
	if(!isset($_SESSION["user_emailid"]))
	{
		header("location: ./");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
            $emailid=$_SESSION["user_emailid"];
            
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where user_emailid='$emailid'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));

            unset($_SESSION["user_id"]);
            unset($_SESSION["user_emailid"]);
            
            header("location: ./");
            exit;
        }

    }
	
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Eikonha :: Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>

<div class="container-fluid">
    <div class="row p-2">
        <div class="col-12">
            <img src="img/logo.png" class="img-fluid" alt=""/> 
        </div>
    </div>
    <div class="row p-1">
        <div class="col-12 text-right">
            <a href="?action=logout" class="btn btn-sm btn-danger">Logout</a>
        </div>
    </div>
    <div class="row video-panel">
        <div class="col-12 col-md-8 offset-md-4">
            <div class="embed-responsive embed-responsive-16by9">
              <iframe class="embed-responsive-item" src="video.php" allowfullscreen scrolling="no"></iframe>
            </div>    
        </div>
    </div>
    <div class="row mb-2">    
        <div class="col-12 col-md-4">
            <div class="question-box">
              <form id="question-form" method="post" role="form">
                      <div class="row">
                        <div class="col-10 offset-1">
                            <h6>Ask your Question:</h6>
                            <div id="message" style="display:none;"></div>
                            <div class="form-group">
                               <textarea class="form-control" name="userQuestion" id="userQuestion" required placeholder="Please ask your question" rows="2"></textarea>
                            </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-10 offset-1">
                          <input type="hidden" id="user_empid" name="user_empid" value="<?php echo $_SESSION['user_emailid']; ?>">
                            <input type="submit" class="btn btn-submit" value="Submit" alt="Submit">
                            
                        </div>
                      </div>  
                </form>
            </div>    
        </div>
        
    </div>
    
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(function(){

	$(document).on('submit', '#question-form', function()
    {  
            $.post('submitques.php', $(this).serialize(), function(data)
            {
                if(data=="success")
                {
                  $('#message').removeClass('alert-danger fail');
                  $('#message').addClass('alert-success success'); 
                  $('#message').text('Your question is submitted successfully.').fadeIn().delay(2000).fadeOut();
                  $('#question-form').find("textarea").val('');
                }
                else 
                {
                  $('#message').addClass('alert-danger fail');
                  $('#message').removeClass('alert-success success'); 
                  $('#message').text(data);
                }
                
            });
        
      
      return false;
    });
});
function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
			   /*if(output=="0")
			   {
				   location.href='index.php';
			   }*/
         }
});
}
setInterval(function(){ update(); }, 30000);

</script>
</body>
</html>