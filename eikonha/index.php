<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Eikonha :: Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>

<div class="container-fluid">
    <div class="row p-2">
        <div class="col-12 col-md-3 offset-md-9 text-right">
            <img src="img/logo.png" class="img-fluid" alt=""/> 
        </div>
    </div>
    <div class="row p-2">
        <div class="col-12 col-md-6 text-left">
            <img src="img/body.png" class="img-fluid" alt=""/> 
        </div>
    </div>
  <div class="row p-0 mt-1">
        <div class="col-12 col-md-6 p-0">
            <img src="img/left.png" class="img-fluid" alt=""/>
      </div>
    <div class="col-12 col-md-5 text-center">
        <div class="row">
            <div class="col-12 col-md-8 offset-md-2">
                <small>*Please enter your registered email address</small>
                <form id="login-form" method="post" role="form">
                    <div id="login-message"></div>
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Enter Email Address" name="emailid" id="emailid" required>
                    </div>
                  <div class="row form-group">
                      <div class="col-md-6">
                        <h6><a href="https://www.aestheticknowledge.com/" target="_blank">Register</a> </h6>
                      </div>
                      <div class="col-md-6">  
                      <input type="image" src="img/btn-submit.png" value="Submit">
                      </div>
                    </div>
                    
                </form> 
            </div>
        </div>   
        <div class="row p-1">
            <div class="col-12">
                <script src="https://apps.elfsight.com/p/platform.js" defer></script>
                <div class="elfsight-app-fa0da63b-1958-4373-8883-0a89466ed8fa"></div>
            </div>
        </div>     
        <div class="row p-2">
            <div class="col-12">
                For queries, please send an email to: info@eikonha.com
            </div>
        </div>     
      </div>
  </div>
    
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).on('submit', '#login-form', function()
{  
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
     // console.log(data);
      if(data == 's')
      {
        window.location.href='webcast.php';  
      }
      else if (data == '-1')
      {
          $('#login-message').text('You are already logged in. Please logout and try again.');
          $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
      else if (data == '0')
      {
          $('#login-message').text("You are not registered.");
          $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
  });
  
  return false;
});
</script>
</body>
</html>